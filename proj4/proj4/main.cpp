//
//  main.cpp
//  proj4
//
//  Created by Adilet Issayev on 03/10/2015.
//  Copyright © 2015 Adilet Issayev. All rights reserved.
//

#include <iostream>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <string.h>


using namespace std;

void DieWithError(string errorMessage);
void printUsage (const char * argv[]);
void* updateStats (void *arg);
bool textFileCheck (string path);
void printStats ();

#define MAXTHREADS 15

//stats
int badFiles = 0;
int dirNum = 0;
int regFilesNum = 0;
int specFilesNum = 0;
int regFileBytes = 0;
int txtFilesNum = 0;
int txtFilesBytes = 0;


//threads part
pthread_t thread_id[MAXTHREADS];
sem_t *sem;

int main(int argc, const char * argv[]) {
	

	int thread_flag = 0;		// 0 for serial, 1 for multithreaded
	int threadNum;		// number of threads for multithreaded architecture
	struct timeval start, end;
	
	gettimeofday(&start, NULL);
	
	vector <string> *input = new vector<string>();
	
	if (argc == 1) {
	
		thread_flag = 0;
	}
	else if (argc == 3) {
		// check for right multithread option usage
		if ( strcmp(argv[1], "thread") != 0 || atoi(argv[2]) > MAXTHREADS || atoi(argv[2]) <= 0) {
			printUsage(argv);
			DieWithError("Wrong usage");
		}
		
		thread_flag = 1;
		threadNum = atoi(argv[2]);
	}
	else {
		printUsage(argv);
		DieWithError("Wrong number of arguments");
	}
	
	
	
	string line;
	// fill in vector<string> input with files
	while (getline(cin, line))
	{
		input->push_back(line);
	}
	
	// TODO wall clock time
	
	//semaphore open
	
	if (thread_flag == 1) {
		sem = sem_open(string("/sem").c_str(), O_CREAT, 0666, 1);
	}
	
	
	for(vector<string>::size_type i = 0; i != input->size(); i++) {
		cout << "Vector " << input->at(i) << endl;
		
		if (thread_flag == 1) {
			
			if(pthread_create( &thread_id[i], NULL, updateStats, &input->at(i))) {
				DieWithError("Could not create thread");
			}
		}
		else {
			
			updateStats(&input->at(i));
		}
		
		
	}
	
	if (thread_flag == 1) {
		
		for (int j = 0; j < input->size(); j++) {
			pthread_join(thread_id[j], NULL);
		}
	}
	
	

	sem_close(sem);
	
	sem_unlink(string("/sem").c_str());
	
	
	
	printStats();
	
	// calculate time
	gettimeofday(&end, NULL);
	int wallclockiIme = (end.tv_usec - start.tv_usec)*0.001;
	rusage stats;
	getrusage(0, &stats);
	
	cout << "\n\nTime used: " << endl;
	cout << "\tWall-clock time: \t" << wallclockiIme << " in ms" << endl;
	cout << "\tSystem time: \t\t" << stats.ru_stime.tv_usec / 1000 << " in ms" << endl;
	cout << "\tUser time: \t\t" << stats.ru_utime.tv_usec / 1000 << " in ms" << endl;
	
	
	
    return 0;
}


void* updateStats (void *arg) {
	
	struct stat buf;
	string path = *(string *)arg;

	//Bad Files , error on stat return
	if ( stat(path.c_str(), &buf) == -1) {
		sem_wait(sem);
		badFiles++;
		sem_post(sem);
	}
	else {
		// isDirectory
		if ( S_ISDIR(buf.st_mode)) {
			sem_wait(sem);
			dirNum++;
			sem_post(sem);
		}
		// isRegularFile
		else if (S_ISREG(buf.st_mode)) {
			sem_wait(sem);
			regFilesNum++;
			regFileBytes += buf.st_size;
			sem_post(sem);
			
			//isTextFile
			if (textFileCheck (path)) {
				sem_wait(sem);
				txtFilesNum++;
				txtFilesBytes +=buf.st_size;
				sem_post(sem);
			}
			
			
		}
		// isSpecialFile
		else {
			sem_wait(sem);
			specFilesNum++;
			sem_post(sem);
		}
			
	}
	
	return NULL;
	
}


void DieWithError(string errorMessage) {
	cerr << errorMessage << endl;
	exit(1);
}

void printUsage(const char *argv[]){
	
	cout << "Program4 Usage: "<< endl;
	cout << "\t\t (1) " << argv[0] << " \t [default, with no parameters] " << endl;
	cout << "\t\t (2) " << argv[0] << " thread <number of threads> [Max is 15] " << endl;
}

bool textFileCheck (string path) {
	
	char buf[1];
	int filedis;
	size_t cnt;
	
	if ((filedis = open(path.c_str(), O_RDONLY)) < 0) {
		return false;
	}
	
	while ((cnt = read(filedis, buf, 1)) > 0) {
		if ( !(isprint(buf[0]) || isspace(buf[0])) ) {
			close(filedis);
			return false;
		}
	}
	
	close(filedis);
	return true;
	
}


void printStats () {
	
	cout << "File Stats: " << endl;
	cout << "\tBad Files: \t\t" << badFiles << endl;
	cout << "\tDirectories: \t\t" << dirNum << endl;
	cout << "\tRegular Files: \t\t" << regFilesNum << endl;
	cout << "\tSpecial Files: \t\t" << specFilesNum << endl;
	cout << "\tRegular File Bytes: \t" << regFileBytes << endl;
	cout << "\tText Files: \t\t" <<  txtFilesNum<< endl;
	cout << "\tText File Bytes: \t" << txtFilesBytes << endl;

}
