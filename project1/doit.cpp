// name: 		Adilet Issayev
// username: 	aissayev

#include <iostream>
using namespace std;
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


extern char **environ;
#define BUFF_SIZE 128

int main (int argc, char **argv)
{

	// declare variables
	char *argvNew[BUFF_SIZE];
	char cmd[BUFF_SIZE];

	int p_id;
	int i;
	// ========================================================================

	// print out the arguments

	cout << "There are " << argc << " arguments: \n";

	// print out arguments
	for (i = 0; i < argc; i++ )
	{
		cout << argv [i] << "\n";
	}

	while (1)
	{
		printf("doit$=>>");

		// read input commands
		fgets(cmd, BUFF_SIZE, stdin);
		// make sure there is null terminator
		cmd[strlen(cmd)-1] = "\0";

		// create a process
		if ((p_id = fork()) < 0)
		{
			cerr << "Fork error\n";
			exit(1);
		}
		else if (p_id == 0)
		{
			// child process
	/*		argvNew[0] = "/bin/ls";
			argvNew[1] = "-l";
			argvNew[2] = NULL;*/

			if (execve(argvNew[0], argvNew, environ) < 0)
			{
				cerr << "Execve error\n";
				exit(1);
			}
			exit (0);
		}
		else 
		{
			// parent
			wait(0);
		}
	}




}