//
//  rat.cpp
//  maze
//
//  Created by Adilet Issayev on 26/09/2015.
//  Copyright © 2015 Adilet Issayev. All rights reserved.
//

#include "rat.hpp"
#include "Siri.hpp"
#include <iostream>


using namespace std;


Rat::Rat(int id, queue<int> *path):
id(id), // automatic set variable
path(path)
{}

/* <returnType> <nameSpace> <function>*/
void Rat::startRun(bool isblocking) {
	
	if (isblocking) {
		// while queue is not empty
		while (!this->path->empty()) {
			
			//enter rat to ther room
			Siri::Instance()->rooms[this->path->front()]->enterRoom(this);
			this->path->pop();
		}
	}
	else {
		while (!this->path->empty()) {
			
			// try to enter every room on rats way
			
			if(!this->path->empty()) {
				Siri::Instance()->rooms[this->path->front()]->enterRoom(this);
				this->path->pop();
			}
			else {
				this->path->push(this->path->front());
				this->path->pop();
				
			}
			//enter rat to ther room
		}

		
	}
	
	markCompletion();
}

void Rat::markCompletion() {
	this->roomFinishTime = Siri::Instance()->timeDiff();
	Siri::Instance()->updateMazeTraversalTime(this);
}

// completed one room
double Rat::getTraversalTime() {
	return roomFinishTime;
}



