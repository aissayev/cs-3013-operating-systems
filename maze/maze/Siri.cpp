//
//  Siri.cpp
//  maze
//
//  Created by Adilet Issayev on 30/09/2015.
//  Copyright © 2015 Adilet Issayev. All rights reserved.
//

#include "Siri.hpp"
#include <iostream>
#include <sstream>



Siri* Siri::m_pInstance = NULL;
time_t Siri::bootTime = time(NULL);

Room* Siri::rooms[MAXROOMS];
Rat* Siri::rats[MAXRATS];
int Siri::ratsInMaze;
int Siri::roomNum;
double Siri::mazeTraversalTime;

Siri* Siri::Instance(){
	
	if (!m_pInstance) {
		m_pInstance = new Siri;
		
	}
	return m_pInstance;
}

double Siri::timeDiff() {
	
	return difftime(time(NULL), bootTime);
}

void Siri::updateMazeTraversalTime(Rat *rat) {
	
	mazeTraversalTime = mazeTraversalTime + rat->roomFinishTime;
}


void Siri::printRatStats() {
	
	for (int i = 0; i < ratsInMaze; i++) {
		cout <<	"Rat " << rats[i]->id << " completed maze in " << rats[i]->getTraversalTime() << " seconds." << endl;
	}
	

	
}

void Siri::printFinalStats() {
	
	int totalDelay = 0;

	
	printRatStats();
	
	for (int i = 0; i < roomNum; i++) {
		
		ostringstream entries;
		
		totalDelay = totalDelay + rooms[i]->delay;
		
		
		for (int j = 0; j < ratsInMaze; j++) {
			
			entries << rooms[i]->printBookEntry(j);
			
		}
		
		cout << "Room " << rooms[i]->id << " [" << rooms[i]->capacity << " "<< rooms[i]->delay << "]: " << entries.str() <<  endl;
		
		
		
	}
	
	cout << "Total traversal time: " << mazeTraversalTime << " seconds, compared to ideal time: " << totalDelay * ratsInMaze << " seconds." << endl;
	
	
}

