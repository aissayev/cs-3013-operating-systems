//
//  Siri.hpp
//  maze
//
//  Created by Adilet Issayev on 30/09/2015.
//  Copyright © 2015 Adilet Issayev. All rights reserved.
//

#ifndef Siri_hpp
#define Siri_hpp

#include <stdio.h>
#include <time.h>
#include "room.hpp"
#include "rat.hpp"

#define MAXROOMS 8
#define MAXRATS 5

class Siri {
public:
	static Siri* Instance();
	static time_t bootTime;
	static double timeDiff();
	static void printRatStats();
	static void updateMazeTraversalTime(Rat *rat);
	static void printFinalStats();
	
	static Room *rooms[MAXROOMS];
	static Rat *rats[MAXRATS];
	static int ratsInMaze;
	static double mazeTraversalTime;
	static int roomNum;
	
	
private:
	Siri(){};
	Siri(Siri const&){};
	Siri& operator= (Siri const&);
	static Siri* m_pInstance;
	
};



#endif /* Siri_hpp */
