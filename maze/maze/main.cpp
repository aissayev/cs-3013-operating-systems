//
//  main.cpp
//  maze
//
//  Created by Adilet Issayev on 26/09/2015.
//  Copyright © 2015 Adilet Issayev. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
#include <pthread.h>

#include "rat.hpp"
#include "room.hpp"
#include "Siri.hpp"



// initialize rooms and rats
// create threads
// let them run
// write stats

using namespace std;

/* function prototypes*/
void printUsage ();
void DieWithError(string errorMessage);
int createRooms ();
void createRats(int roomNum, string algorithm);

void createThreads (int ratNum);
void* ratRace(void *argc);
void printRoomStats ();

int ratNum;
int roomNum;

bool blocking_flag = true;

/* main */
int main(int argc, const char * argv[]) {

	if (argc != 3) {
		printUsage();
		DieWithError("Incorrect number of arguments. Try Again! ;)");
	}
	
	
	string algorithm; // Algorithm
	
	
	/* check rats validity*/
	ratNum = atoi(argv[1]);
	
	if (ratNum < 1 || ratNum > 5) {
		DieWithError("Not valid number of rats in the maze. Try Again ;)");
	}
	
	/* check algorithm validity*/
	algorithm = argv[2];
	
	
	if (!(algorithm.compare("i") == 0 || algorithm.compare("d") == 0 || algorithm.compare("n")== 0)) {
		DieWithError("No such algorithm, Try Again");
	}
	
	cout << "\nNumber of rats is " << ratNum << endl;
	cout << "Type of algorithm is " << algorithm << endl;
	cout << "\talgorithm: 'i' in-order, 'd' distributed, 'n' non-blocking" << endl;
	
	
	
	//start time
	Siri::Instance()->bootTime = time(NULL);
	
	
	
	roomNum = createRooms();
	
	cout << "\nNumber of rooms is " << roomNum << endl << endl << endl;
	
	Siri::Instance()->ratsInMaze = ratNum;
	
	Siri::Instance()->roomNum = roomNum;
	
	createRats(roomNum, algorithm);
	
	createThreads(ratNum);
	
	
	Siri::Instance()->printFinalStats();

	
    return 0;
	
	
}

/* function definitions */
void printUsage () {
	
	cout << "Usage: ./maze <rats> <algorithm>" << endl;
	cout << "	rats from 1 to 5" << endl;
	cout << "	algorithm: 'i' in-order, 'd' distributed, 'n' non-blocking" << endl;
}

void DieWithError(string errorMessage) {
	cerr << errorMessage;
	cout << endl;
	exit(1);
}

// createRooms from "rooms" input file
int createRooms () {
	

	/* Open rooms files*/
	ifstream infile ("rooms");
	if(!infile.is_open()){
		DieWithError("Could not open rooms");
	}

	/* create rooms from info*/
	int capacity = 0, delay = 0;
	int roomCount = 0;
	int id = 0;
	
	while (infile >> capacity >> delay)
	{
	
		roomCount++;
		
		if(roomCount > MAXROOMS) {
			DieWithError("Exceeded max number of rooms");
		}
		
		Siri::rooms[id] = new Room(id, capacity, delay);
		
		//rooms[id]->printRoom();
		id++;

	}
	
	infile.close();
	return roomCount;
	
}

void createRats(int roomNum, string algorithm) {
	for (int i = 0; i < ratNum; i++) {
		
		//inititate queue
		queue<int> *path = new queue<int>();
		
		/* Algorithm Selection*/
		if(algorithm.compare("i") == 0) {
			
			for (int j = 0; j < roomNum; j++) {
				path->push(j);
			}
			
		}
		else if(algorithm.compare("d") == 0) {
			
			//distributedAlg();
			int d = ratNum > roomNum ? i % roomNum : i;
			for (int k = 0;  k < roomNum; k++) {
				path->push(d);
				d++;
				if (d >= roomNum ){
					d = 0;
				}
			}
		}
		else if(algorithm.compare("n") == 0) {
			
			//nonblockingAlg();
			blocking_flag = false;
			
			int d = ratNum > roomNum ? i % roomNum : i;
			for (int k = 0;  k < roomNum; k++) {
				path->push(d);
				d++;
				if (d >= roomNum ){
					d = 0;
				}
			}
			
		}
		
		
		Siri::rats[i] = new Rat (i, path);
	}
}

void createThreads (int ratNum) {
	
	pthread_t thread_id[MAXRATS];
	int rat_id[MAXRATS] = {0, 1, 2, 3, 4};
	
	for (int i = 0; i < ratNum; i++) {
		
		if(pthread_create( &thread_id[i], NULL, ratRace, &rat_id[i])) {
			DieWithError("Could not create thread");
		};
	}
	
	for (int j = 0; j < ratNum; j++) {
		pthread_join(thread_id[j], NULL);
	}
	
}

void* ratRace(void *arg) {
	
	
	int id = *(int *)arg;
	Siri::Instance()->rats[id]->startRun(blocking_flag);

	return NULL;
}




/*
 int pthread_create(
	pthread_t *thread,  //
	const pthread_attr_t *attr, // NULL for default
	void *(*start_routine) (void *), //
	void *arg);
 
 @return 0 if successfull
 */

