//
//  rat.hpp
//  maze
//
//  Created by Adilet Issayev on 26/09/2015.
//  Copyright © 2015 Adilet Issayev. All rights reserved.
//

#ifndef rat_hpp
#define rat_hpp

#include <stdio.h>
#include <queue>

using namespace std;

class Rat{
public:
	int id;
	queue<int> *path;
	double roomFinishTime;
	
	double getTraversalTime();
	
	
	Rat(int id, queue<int> *path);
	
	void startRun(bool isblocking); // initiates run for the rat
	void markCompletion();
};

#endif /* rat_hpp */
