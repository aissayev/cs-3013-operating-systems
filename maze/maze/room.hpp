//
//  room.hpp
//  maze
//
//  Created by Adilet Issayev on 26/09/2015.
//  Copyright © 2015 Adilet Issayev. All rights reserved.
//

#ifndef room_hpp
#define room_hpp

#include <stdio.h>
#include <semaphore.h>
#include "rat.hpp"

#define MAXRATS 5

struct VBEntry {
	int iRat;
	int tEntry;
	int tDep;
};

class Room {
public:
	int id;
	int capacity;
	int delay;
	
	sem_t *bookSem;
	sem_t *roomEnterSem;
	
	
	VBEntry visitorsBook[MAXRATS];
	
	int numRatsInRoom;
	
	// constructor
	Room (int id, int capacity, int delay);
	//destructor
	~Room();

	
	void addBookEntry (int, int, int);
	
	// let rat enter the room
	void enterRoom (Rat *rat);
	
	//void printRoom ();
	
	string printBookEntry(int i);
};



#endif /* room_hpp */
