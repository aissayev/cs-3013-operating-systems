//
//  room.cpp
//  maze
//
//  Created by Adilet Issayev on 26/09/2015.
//  Copyright © 2015 Adilet Issayev. All rights reserved.
//

/* <returnType> <nameSpace> <function>*/

#include "room.hpp"
#include "Siri.hpp"
#include <string>
#include <iostream>
#include <sstream>



using namespace std;

Room:: Room(int id, int capacity, int delay):
id(id),
capacity(capacity),
delay(delay),
// bookSem to let only one rat write into visitorBook
bookSem(sem_open((string("/bookSem") + string(to_string(static_cast<long long>(id)))).c_str(), O_CREAT, 0666, 1)),
roomEnterSem(sem_open((string("/roomEnterSem") + string(to_string(static_cast<long long>(id)))).c_str(), O_CREAT, 0666, capacity))

{}


void Room::addBookEntry(int rat_id, int tEntry, int tDep) {
	
	// wait for semaphore
	sem_wait(bookSem);
	
	visitorsBook[rat_id] = VBEntry { rat_id, tEntry, tDep};
	
	// signal for semaphore
	sem_post(bookSem);
	
	
	
}

void Room::enterRoom(Rat *rat) {
	
	sem_wait(roomEnterSem);
	// enter time
	double enterTime = Siri::Instance()->timeDiff();
	sleep(delay);
	//add entry for the rat
	addBookEntry(rat->id, enterTime, Siri::Instance()->timeDiff());
	
	sem_post(roomEnterSem);
}

/*
void Room::printRoom() {

	ostringstream entries;
	
	int numEntries = Siri::Instance()->ratsInMaze;
	for (int i = 0; i < numEntries; i++) {
		
		entries << printBookEntry(i);
		
	}
	
	cout << "Room " << id << " [" << capacity << " "<< delay << "]: " << entries.str() <<  endl;
	
}
*/

string Room::printBookEntry(int i) {
	
	ostringstream bookEntryInfo;
	
	bookEntryInfo << visitorsBook[i].iRat << " " << visitorsBook[i].tEntry << " " << visitorsBook[i].tDep << "; ";
	
	return bookEntryInfo.str();
	
}

Room::~Room(){
	
	sem_close(bookSem);
	sem_close(roomEnterSem);
	
	sem_unlink((string("/roomEnterSem") + string(to_string(static_cast<long long>(id)))).c_str());
	sem_unlink((string("/bookSem") + string(to_string(static_cast<long long>(id)))).c_str());

}